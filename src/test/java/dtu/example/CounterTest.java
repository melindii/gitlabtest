package dtu.example;

import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;
//import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CounterTest {
    @Test
    public void twoPlusTwoEqualFour(){
        var count = new Counter();
        assertEquals(4,count.add(2, 2));
    }
}